package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    public KnightAdventurer(){
        AttackBehavior attackBehavior = new AttackWithSword();
        DefenseBehavior defenseBehavior = new DefendWithArmor();
        setAttackBehavior(attackBehavior);
        setDefenseBehavior(defenseBehavior);
    }

    @Override
    public String getAlias() {

        return "Knight";
    }
    //ToDo: Complete me
    //ToDo: Complete me
}
