package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    @Override
    public String attack() {
        return "Slash!!!";
    }
    //ToDo: Complete me

    @Override
    public String getType() {
        return "Attack With Sword";
    }
}
