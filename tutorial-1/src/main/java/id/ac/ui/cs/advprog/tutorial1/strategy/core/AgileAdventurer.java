package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    public AgileAdventurer(){
        AttackBehavior attackBehavior = new AttackWithGun();
        DefenseBehavior defenseBehavior = new DefendWithBarrier();
        setAttackBehavior(attackBehavior);
        setDefenseBehavior(defenseBehavior);
    }

    @Override
    public String getAlias() {
        return "Agile";
    }
    //ToDo: Complete me
}
