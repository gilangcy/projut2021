package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    @Override
    public String defend() {
        return "CRASH!!";
    }

    @Override
    public String getType() {
        return "Defend With Armor";
    }
    //ToDo: Complete me
}
