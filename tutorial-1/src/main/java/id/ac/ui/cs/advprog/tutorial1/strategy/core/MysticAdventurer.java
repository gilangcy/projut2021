package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    public MysticAdventurer(){
        AttackBehavior attackBehavior = new AttackWithMagic();
        DefenseBehavior defenseBehavior = new DefendWithShield();
        setAttackBehavior(attackBehavior);
        setDefenseBehavior(defenseBehavior);
    }

    @Override
    public String getAlias() {

        return "Mystic";

    }
    //ToDo: Complete me
    //ToDo: Complete me
}
