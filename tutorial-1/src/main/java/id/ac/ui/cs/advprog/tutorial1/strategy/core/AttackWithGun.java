package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String attack() {
        return "DOR..DOR..BOOM!!";
    }
    //ToDo: Complete me

    @Override
    public String getType() {
        return "Attack With Gun";
    }
}
